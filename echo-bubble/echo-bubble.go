package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var echoServer, echoPort string

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("--- indexHandler")
	r.ParseForm()
	input, exists := r.Form["input"]
	if !exists {
		log.Println("No INPUT")
		w.Write([]byte("ECHO BUBBLE ERROR: No INPUT"))
		return
	}
	// bubble request to internal echo server
	u := "http://" + echoServer + ":" + echoPort + "/?input=" + input[0]
	log.Println("Sending request to echo server", u)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u, nil)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error making request", u)
		w.Write([]byte("ECHO BUBBLE ERROR: No Response from ECHO"))
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading response", u)
		w.Write([]byte("ECHO BUBBLE ERROR: Could not read response from ECHO"))
		return
	}

	w.Write([]byte(string(body) + " BAR"))
}

func initServer(host, port string) error {
	http.HandleFunc("/", indexHandler)
	return http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), nil)
}

func main() {

	host := os.Getenv("ECHO_BUBBLE_HOST")
	port := os.Getenv("ECHO_BUBBLE_PORT")
	echoServer = os.Getenv("ECHO_HOST")
	echoPort = os.Getenv("ECHO_PORT")

	if host == "" || port == "" || echoServer == "" || echoPort == "" {
		log.Println("main: env var not set")
		return
	}

	err := initServer(host, port)
	if err != nil {
		log.Println("Error starting server:", err)
	}
}
