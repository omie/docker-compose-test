docker-compose-test
-------------------

tiny programs to test docker-compose


echo service : Simply echo's input by adding special word 'FOO' to it

echo-bubble : Accepts input -> sends it to echo service -> adds 'BAR' to the response and returns it

For running echo,
    export ECHO_HOST=host address to bind to
    export ECHO_PORT=port address to bind to

For running echo-bubble,
    export ECHO_BUBBLE_HOST=host address to bind to
    export ECHO_BUBBLE_PORT=port address to bind to
    export ECHO_HOST=host address to point to echo
    export ECHO_PORT=port address to point to echo

Send HTTP GET request to echo bubble,

    http://echo-bubble-host:port/?input=something

Expected Response

    something FOO BAR


Run up command to get the environment in place
	$ docker-compose up




