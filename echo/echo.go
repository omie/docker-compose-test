package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("--- indexHandler")
	r.ParseForm()
	input, exists := r.Form["input"]
	if exists {
		log.Println("input found:", input[0])
		w.Write([]byte(input[0] + " FOO"))
		return
	}
	log.Println("input not given")
	w.Write([]byte("ECHO ERROR: No INPUT"))
}

func initServer(host, port string) error {
	http.HandleFunc("/", indexHandler)
	return http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), nil)
}

func main() {

	host := os.Getenv("ECHO_HOST")
	port := os.Getenv("ECHO_PORT")

	if host == "" || port == "" {
		log.Println("main: env var not set")
		return
	}

	err := initServer(host, port)
	if err != nil {
		log.Println("Error starting server:", err)
	}
}
